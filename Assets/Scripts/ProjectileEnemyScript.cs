﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileEnemyScript : MonoBehaviour
{

    public Rigidbody rigidEnemy;
    public float speed;

    public GameObject bullet;
    Vector2 bulletPos;

    public bool trueIfPlayerDetected;

    float tempTime;
    

    // Update is called once per frame
    void Update()
    {
        if(trueIfPlayerDetected)
        {
            tempTime += Time.deltaTime;
            if (tempTime > 0.6)
            {
                tempTime = 0;
                Fire();
            }
        }
    }



    void Fire()
    {
        bulletPos = transform.position;

        bulletPos += new Vector2(+1f, 0);
        Instantiate(bullet, bulletPos, Quaternion.identity);

    }
}
