﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIScript : MonoBehaviour {
    

    public void ChangeScene(string name)
    {
        SceneManager.LoadSceneAsync(name, LoadSceneMode.Single);
    }

    

    public void ExitGame()
    {
        Application.Quit();
    }


}
