﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteBehindPlayerScript : MonoBehaviour {

	
    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "DeadByFallingOffLevel")
        {
            Physics.IgnoreCollision(col.collider, GetComponent<BoxCollider>());
        }
        else
        Destroy(col.gameObject);
    }

}
