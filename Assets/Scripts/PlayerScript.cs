﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour {

    Rigidbody playerRigid;

    Vector3 movement;
    public float speed;
    public float speedCopy;

    public float jumpForce;
    public bool inAir;

    public GameObject bullet;
    Vector2 bulletPos;
    bool stop;

    public EnemyScript enemyScript;

    
	// Use this for initialization
	void Start () {
        playerRigid = GetComponent<Rigidbody>();
        speedCopy = speed;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(stop == false)
            {
                if(inAir == false)
                {
                    playerRigid.AddForce(new Vector3(0, jumpForce,0.0f), ForceMode.Impulse);
                    inAir = true;
                }
            }
            else
            {
                stop = false;
            }
        }
        else if(Input.GetKey(KeyCode.LeftControl))
        {
            speed = 0;
        }
        else if(Input.GetKeyUp(KeyCode.LeftControl))
        {
            speed = speedCopy;
        }
        
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            Fire();
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (Input.mousePosition.x < Screen.width / 2)
            {
                if (stop == false)
                {
                    if (inAir == false)
                    {
                        playerRigid.AddForce(new Vector3(0, jumpForce, 0.0f), ForceMode.Impulse);
                        inAir = true;
                    }
                }
                else
                {
                    stop = false;
                }
            }
            else if (Input.mousePosition.x > Screen.width / 2)
            {
                Fire();
            }
        }
        else
        {
            //Do nothing for now
        }
        if (stop == false)
        {
            movement = new Vector3(1,0,0);
            playerRigid.transform.Translate(movement * speed * Time.deltaTime);
        }
            playerRigid.AddForce(Physics.gravity, ForceMode.Acceleration);
        
    }



    void OnCollisionEnter(Collision col)
    {
        if (col.collider.tag == "ground" || col.collider.tag == "UpDown" || col.collider.tag == "LeftRight")
        {
            inAir = false;
        }
        else if (col.gameObject.tag == "border")    
        {
            Physics.IgnoreCollision(col.collider, GetComponent<CapsuleCollider>());
        }
        else if (col.gameObject.tag == "StopPlate")
        {
            stop = true;
            
        }
        else if(col.gameObject.name == "DetectionForProjectileEnemy")
        {
            Physics.IgnoreCollision(col.collider, GetComponent<CapsuleCollider>());
            
            GameObject enemy = col.transform.parent.gameObject;

            enemy = enemy.transform.Find("Enemy").gameObject;
            ProjectileEnemyScript script = enemy.GetComponent<ProjectileEnemyScript>();
            script.trueIfPlayerDetected = true;
            Debug.Log("Detected enemy collision");
        }
        else if(col.gameObject.tag == "DeadByFallingOffLevel" || col.gameObject.tag == "Enemy")
        {
            Dead();
        }
    }



    void OnCollisionExit(Collision col)
    {
        if(col.collider.tag == "ground")
        {
            inAir = true;
        }
    }


    void Fire()
    {
        bulletPos = transform.position;

        bulletPos += new Vector2(+1f, 0);
        Instantiate(bullet, bulletPos, Quaternion.identity);
    }


    void Dead()
    {
        SceneManager.LoadSceneAsync("DeathScene");
    }
}
