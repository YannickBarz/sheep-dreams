﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlat : MonoBehaviour {

    Rigidbody platRigid;
    public Vector3 direction;
    public float speed;
    public bool updown;

    public BoxCollider border1, border2;

	// Use this for initialization
	void Start () {
        platRigid = GetComponent<Rigidbody>();

        if(updown)
        {
            direction = new Vector3(0, 1, 0);
            border1.transform.position = new Vector3(platRigid.transform.position.x, platRigid.transform.position.y + 5, platRigid.transform.position.z);
            border2.transform.position = new Vector3(platRigid.transform.position.x, platRigid.transform.position.y - 5, platRigid.transform.position.z);
        }
        else
        {
            direction = new Vector3(1, 0, 0);
            border1.transform.position = new Vector3(platRigid.transform.position.x - 10, platRigid.transform.position.y, platRigid.transform.position.z);
            border2.transform.position = new Vector3(platRigid.transform.position.x + 10, platRigid.transform.position.y, platRigid.transform.position.z);
        }
	}
	
	// Update is called once per frame
	void Update () {
        platRigid.transform.Translate(direction * speed * Time.deltaTime);
        
    }



    void OnCollisionEnter(Collision col)
    {
        if(col.collider.tag == "border")
        {
            speed = speed * -1;
        }
        else if(col.collider.name == "Player")
        {
            //https://www.youtube.com/watch?v=O6wlIqe2lTA
            col.collider.transform.SetParent(transform);
        }
    }



    void OnCollisionExit(Collision col)
    {
        if(col.collider.name == "Player")
        {
            //https://www.youtube.com/watch?v=O6wlIqe2lTA
            col.collider.transform.SetParent(null);
        }
    }
}
