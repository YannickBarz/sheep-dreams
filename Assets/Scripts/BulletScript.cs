﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    public float speedX = 20, speedY = 0;
    public Rigidbody2D rigidBullet;

	
	// Update is called once per frame
	void Update () {
        rigidBullet.velocity = new Vector2(speedX, speedY);
	}
}
