﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionForProjectileEnemyScript : MonoBehaviour {

    public GameObject enemy;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Player")
        {
            ProjectileEnemyScript script = enemy.GetComponent<ProjectileEnemyScript>();
            script.trueIfPlayerDetected = true;
            Debug.Log("Detected enemy collision");
        }
    }
}
