﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {
    
    public Rigidbody rigidEnemy;
    Vector3 movement;
    public float speed = 3f;

    Vector3 copy;
    void Start()
    {
        copy = rigidEnemy.transform.position;
        movement = new Vector3(1, 0, 0);
    }

    void Update()
    {
        if (rigidEnemy.transform.position.x < copy.x - 3 || rigidEnemy.transform.position.x > copy.x + 3)
            speed *= -1f;

        rigidEnemy.transform.Translate(movement * speed * Time.deltaTime);
    }

    
    //public IEnumerator Attack()
    //{
    //    Vector3 copy = fist.transform.position;
    //    if(fist.transform.position.x >= fist.transform.position.x - 3)
    //    {
    //        rigidFist.transform.Translate(new Vector3(1,0, 0)*speed);
            

    //    }
    //    yield return null;

    //    if (fist.transform.position.x != copy.x)
    //    {
    //        rigidFist.transform.Translate(new Vector3(-1, 0, 0) * speed * Time.deltaTime);
    //    }

    //}
}
