﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour {

    public GameObject toLookAt;

    Vector3 offset;


    // Use this for initialization
    void Start () {
        offset = new Vector3(6f, 2.5f, -2.5f);
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = toLookAt.transform.position + offset;
    }
}
